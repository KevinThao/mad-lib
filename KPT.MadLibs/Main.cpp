
// Luba and Kevin
// Lab Assignment 3: Mad Lib

#include <iostream>
#include <string>
#include <conio.h>
#include <vector>
#include <fstream>

using namespace std;

void MadLib(string* values, ostream& os = cout)
{
	os	<< "\nOne day my " << values[0] << " friend and I decided to go to the " << values[1] << " game in " << values[2] << ".\n"
	<< "We really wanted to see " << values[3] << " play.\n"
	<< "So we " << values[4] << " in the " << values[5] << " and headed down to the " << values[6] << " and bought some " << values[7] << ".\n"
	<< "We watched the game and it was " << values[8] << ".\n"
	<< "We ate some " << values[9] << " and drank some " << values[10] << ".\n"
	<< "We had a " << values[11] << " time, and can't wait to go again.\n";
}

int main()
{
	char input;
	ofstream ofs("madlib.txt");

	// prompt user for at least 10 values for a mad lib (can be vague or specific)

	// must store the data in an array or vector
	const int NUM_VALUES = 12;
	string values[NUM_VALUES];

	string prompts[NUM_VALUES] = { "Enter an adjective (describing word): ", "Enter a sport: ", "Enter a city: ", "Enter a person: ",
		"Enter an action verb: ", "Enter a vehicle: ", "Enter a place: ", "Enter a noun (thing, plural): ", "Enter an adjective (describing word): ", "Enter a food (plural): ",
		"Enter a liquid: ", "Enter an adjective(describing word): " };

	// must use a loop to collect the values
	for (int i = 0; i < NUM_VALUES; i++)
	{
		cout << prompts[i];
		cin >> values[i];

	}

	/*
	cout << "\n\n";
	cout << "One day my " << values[0] << " friend and I decided to go to the " << values[1] << " game in " << values[2] << ".\n";
	cout << "We really wanted to see " << values[3] << " play.\n";
	cout << "So we " << values[4] << " in the " << values[5] << " and headed down to the " << values[6] << " and bought some " << values[7] << ".\n";
	cout << "We watched the game and it was " << values[8] << ".\n";
	cout << "We ate some " << values[9] << " and drank some " << values[10] << ".\n";
	cout << "We had a " << values[11] << " time, and can't wait to go again.";
	*/

	// as if the user would like to save output to file
	MadLib(values);
	cout << "\nWould you like to save output to file? (y/n): ";
	cin >> input;
	if (input == 'y' || input == 'Y')
	{
		MadLib(values, ofs);
	}


	(void)_getch;
	return 0;
}